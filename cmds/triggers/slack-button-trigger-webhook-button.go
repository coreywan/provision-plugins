package main

/*
  Name: slack-button-trigger-webhook-button
  Description: Button Action Trigger
  Documentation: Button Action Trigger
  Path: kaholo-trigger-slack-button/src/config.json

const { verifySignature } = require(`./helpers`);
const minimatch = require("minimatch");

async function webhookButton(req, res, settings, triggerControllers) {
  if (req.body.challenge){
    return res.status(200).send(req.body.challenge);
  }
  try {
    const body = JSON.parse(req.body.payload);
    if (body.type !== "interactive_message") return res.status(400).send("not an interactive message");
    const buttons = body.actions.filter(act => act.type === "button");

    triggerControllers.forEach(trigger => {
      if (!verifySignature(req, trigger)) return;
      const {valuePat} = trigger.params;
      buttons.forEach(button => {
        if (valuePat && !minimatch(button.value, valuePat)) return;
        trigger.execute(`Slack Button "${button.value}"`, button);
      })
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }
}

module.exports = {
  webhookButton
};

*/

import (
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
)

func slack_button_trigger_webhook_button(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	return notImplementedMessage(p, l, ma)
}

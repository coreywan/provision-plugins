package main

/*
   Name: zendesk-trigger-webhook
   Description: zendesk webhook
*/

import (
	"encoding/json"
	"fmt"
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
)

type zendeskWebhook struct {
	Title       string `json:"ticket_title"`
	Description string `json:"ticket_description"`
	Id          string `json:"ticket_id"`
	Raw         string `json:"raw"`
}

func zendesk_trigger_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &zendeskWebhook{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}
	body.Raw = string(triggerInput.Data)

	for _, t := range triggerInput.Triggers {
		/*
			params, err := p.GetTriggerParams(t.Name)
			if err != nil {
				l.Errorf("Error getting %s trigger params: %v", err)
				continue
			}

				if anp, ok := params["github-trigger/webhook-pr/secret"]; ok {
					s := anp.(string)
					if s != "" {
						mac := hmac.New(crypto.SHA256.New, []byte(s))
						mac.Write(triggerInput.Data)
						shaHex := hex.EncodeToString(mac.Sum(nil))
						if shaHex != reqSecret[7:] {
							continue
						}
					}
				}
				if anp, ok := params["github-trigger/webhook-pr/repo-name"]; ok {
					s := anp.(string)
					if r, rerr := regexp.Compile(s); rerr == nil {
						if !r.MatchString(reqRepoName) {
							continue
						}
					} else {
						l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
					}
				}
				if anp, ok := params["github-trigger/webhook-pr/to-branch"]; ok {
					s := anp.(string)
					if r, rerr := regexp.Compile(s); rerr == nil {
						if !r.MatchString(reqTargetBranch) {
							continue
						}
					} else {
						l.Errorf("Trigger %s has bad to-branch pattern: %v", rerr)
					}
				}
				if anp, ok := params["github-trigger/webhook-pr/from-branch"]; ok {
					s := anp.(string)
					if r, rerr := regexp.Compile(s); rerr == nil {
						if !r.MatchString(reqSourceBranch) {
							continue
						}
					} else {
						l.Errorf("Trigger %s has bad from-branch pattern: %v", rerr)
					}
				}
				if anp, ok := params["github-trigger/webhook-pr/action-type"]; ok {
					s := anp.(string)
					if s != "any" {
						if r, rerr := regexp.Compile(s); rerr == nil {
							if !r.MatchString(reqActionType) {
								continue
							}
						} else {
							l.Errorf("Trigger %s has bad action-type pattern: %v", rerr)
						}
					}
				}
		*/

		plugin.Publish("trigger", "fired", t.Name, body)
	}

	return triggerResult, nil
}

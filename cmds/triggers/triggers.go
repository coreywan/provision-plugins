package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.go"
//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml"
//go:generate sh -c "drpcli contents document content.yaml > triggers.rst"
//go:generate rm -f process-content content.yaml

import (
	"fmt"
	"gitlab.com/rackn/logger"
	v4 "gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
	"log"
	"os"
)

var (
	version = v4.RSVersion
	def     = models.PluginProvider{
		Name:          "triggers",
		Version:       version,
		PluginVersion: 4,
		AutoStart:     true,
		AvailableActions: []models.AvailableAction{

			{
				Command: "app-optics-trigger-alert-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"app-optics-trigger/alert-webhook/alert-name-pat",

					"app-optics-trigger/alert-webhook/state",
				},
			},

			{
				Command: "azure-monitor-trigger-alert-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"azure-monitor-trigger/alert-webhook/alert-name-pat",

					"azure-monitor-trigger/alert-webhook/alert-severity",

					"azure-monitor-trigger/alert-webhook/include-higher-severity",
				},
			},

			{
				Command: "bitbucket-trigger-webhook-pr",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"bitbucket-trigger/webhook-pr/to-branch",

					"bitbucket-trigger/webhook-pr/from-branch",

					"bitbucket-trigger/webhook-pr/repo-name",

					"bitbucket-trigger/webhook-pr/action-type",
				},
			},

			{
				Command: "bitbucket-trigger-webhook-push",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"bitbucket-trigger/webhook-push/branch-pat",

					"bitbucket-trigger/webhook-push/tag-pat",

					"bitbucket-trigger/webhook-push/repo-name",
				},
			},

			{
				Command: "datadog-trigger-alert_webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"datadog-trigger/alert_webhook/alert_id",
				},
			},

			{
				Command: "dynatrace-trigger-alert_webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"dynatrace-trigger/alert_webhook/state",
				},
			},

			{
				Command: "elementor-trigger-form-submit",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"elementor-trigger/form-submit/form-id",
				},
			},

			{
				Command: "epsagon-trigger-alert_webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{},
			},

			{
				Command: "git-lab-trigger-mr-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"git-lab-trigger/mr-webhook/secret",

					"git-lab-trigger/mr-webhook/to_branch",

					"git-lab-trigger/mr-webhook/from_branch",

					"git-lab-trigger/mr-webhook/repo_url",

					"git-lab-trigger/secret",
				},
			},

			{
				Command: "git-lab-trigger-webhook-push",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"git-lab-trigger/webhook-push/secret",

					"git-lab-trigger/webhook-push/push_branch",

					"git-lab-trigger/webhook-push/repo_url",

					"git-lab-trigger/secret",
				},
			},

			{
				Command: "github-trigger-webhook-pr",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"github-trigger/webhook-pr/secret",

					"github-trigger/webhook-pr/repo-name",

					"github-trigger/webhook-pr/to-branch",

					"github-trigger/webhook-pr/from-branch",

					"github-trigger/webhook-pr/action-type",
				},
			},

			{
				Command: "github-trigger-webhook-push",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"github-trigger/webhook-push/secret",

					"github-trigger/webhook-push/repo-name",

					"github-trigger/webhook-push/branch-pat",

					"github-trigger/webhook-push/tag-pat",
				},
			},

			{
				Command: "jira-trigger-issue-update-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"jira-trigger/issue-update-webhook/status-name",

					"jira-trigger/issue-update-webhook/project-key",
				},
			},

			{
				Command: "jira-trigger-new-issue-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"jira-trigger/new-issue-webhook/project-key",
				},
			},

			{
				Command: "kaholo-trigger-lacework-alert-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"kaholo-trigger-lacework/alert-webhook/event-type",

					"kaholo-trigger-lacework/alert-webhook/id",

					"kaholo-trigger-lacework/alert-webhook/event-severity",

					"kaholo-trigger-lacework/alert-webhook/include-higher-sev",
				},
			},

			{
				Command: "logic-monitor-trigger-alert-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"logic-monitor-trigger/alert-webhook/status",

					"logic-monitor-trigger/alert-webhook/alert-type",

					"logic-monitor-trigger/alert-webhook/admin",

					"logic-monitor-trigger/alert-webhook/id-pat",

					"logic-monitor-trigger/alert-webhook/host-pat",

					"logic-monitor-trigger/alert-webhook/group-pat",

					"logic-monitor-trigger/alert-webhook/level-pat",
				},
			},

			{
				Command: "logzio-trigger-alert_webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"logzio-trigger/alert_webhook/alert_title",

					"logzio-trigger/alert_webhook/severity",
				},
			},

			{
				Command: "monday-trigger-status_update_webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"monday-trigger/status_update_webhook/board_id",

					"monday-trigger/board-id",
				},
			},

			{
				Command: "oci-monitoring-trigger-alert-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"oci-monitoring-trigger/alert-webhook/alert-name-pat",

					"oci-monitoring-trigger/alert-webhook/min-alert-severity",

					"oci-monitoring-trigger/alert-webhook/max-alert-severity",
				},
			},

			{
				Command: "okta-trigger-alert",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"okta-trigger/alert/alert-event-types",

					"okta-trigger/alert/alert-severity",

					"okta-trigger/alert/include-higher-severity",
				},
			},

			{
				Command: "okta-trigger-verify",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{},
			},

			{
				Command: "pager-duty-trigger-incident_webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"pager-duty-trigger/incident_webhook/event",

					"pager-duty-trigger/incident_webhook/urgency",
				},
			},

			{
				Command: "pingdom-trigger-webhook-alert",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"pingdom-trigger/webhook-alert/name-pat",

					"pingdom-trigger/webhook-alert/check-type",
				},
			},

			{
				Command: "prometheus-trigger-alert-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"prometheus-trigger/alert-webhook/alert-name-pat",

					"prometheus-trigger/alert-webhook/group-key-pat",

					"prometheus-trigger/alert-webhook/status",

					"prometheus-trigger/alert-webhook/labels",
				},
			},

			{
				Command: "prtg-trigger-post-notification",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"prtg-trigger/post-notification/sensor-pat",

					"prtg-trigger/post-notification/device-pat",

					"prtg-trigger/post-notification/priority",

					"prtg-trigger/post-notification/include-higher",
				},
			},

			{
				Command: "send-grid-trigger-event",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"send-grid-trigger/event/email-pattern",

					"send-grid-trigger/event/trig-event-type",

					"send-grid-trigger/event/category-pats",

					"send-grid-trigger/public-key",
				},
			},

			{
				Command: "slack-button-trigger-webhook-button",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"slack-button-trigger/webhook-button/signing-secert",

					"slack-button-trigger/webhook-button/value-pat",
				},
			},

			{
				Command: "sumologic-trigger-alert-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"sumologic-trigger/alert-webhook/alert-name",

					"sumologic-trigger/alert-webhook/alert-type",

					"sumologic-trigger/alert-webhook/query-name",
				},
			},

			{
				Command: "unbounce-trigger-webhook-submit",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"unbounce-trigger/webhook-submit/page-name",

					"unbounce-trigger/webhook-submit/page-url",

					"unbounce-trigger/webhook-submit/variant",
				},
			},

			{
				Command: "vsts-trigger-pr-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"vsts-trigger/pr-webhook/repo_url",

					"vsts-trigger/pr-webhook/to_branch",

					"vsts-trigger/pr-webhook/from_branch",
				},
			},

			{
				Command: "vsts-trigger-push-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"vsts-trigger/push-webhook/repo_url",

					"vsts-trigger/push-webhook/push_branch",
				},
			},

			{
				Command: "zabbix-trigger-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"zabbix-trigger/webhook/secret",

					"zabbix-trigger/webhook/severity",
				},
			},

			{
				Command: "zapier-trigger-webhook-post",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{

					"zapier-trigger/webhook-post/key-pat",
				},
			},
			{
				Command: "zendesk-trigger-webhook",
				Model:   "system",
				RequiredParams: []string{
					"trigger/input",
				},
				OptionalParams: []string{},
			},
		},
		Content: contentYamlString,
	}
)

type Plugin struct {
	drpClient *api.Client
	name      string
}

func (p *Plugin) GetTriggerParams(name string) (map[string]interface{}, error) {
	session := p.drpClient
	req := session.Req().UrlFor("triggers", name, "params")
	req.Params("aggregate", "true")
	req.Params("decode", "true")
	res := map[string]interface{}{}
	return res, req.Do(&res)
}

func (p *Plugin) Config(_ logger.Logger,
	session *api.Client,
	config map[string]interface{}) *models.Error {
	var err *models.Error
	p.drpClient = session
	p.name, err = utils.ValidateStringValue("Name", config["Name"])
	return err
}

func getTriggerInput(params map[string]interface{}) (*models.TriggerInput, *models.Error) {
	tiObj, ok := params["trigger/input"]
	if !ok {
		return nil, utils.MakeError(500, "Missing trigger input")
	}
	triggerInput := &models.TriggerInput{}
	if err := models.Remarshal(tiObj, &triggerInput); err != nil {
		return nil, utils.MakeError(500, fmt.Sprintf("Error decoding trigger Input: %v", err))
	}
	return triggerInput, nil
}

func notImplementedMessage(p *Plugin, l logger.Logger, ma *models.Action) (*models.TriggerResult, *models.Error) {
	triggerResult := &models.TriggerResult{
		Data:   []byte("Not Implemented"),
		Code:   500,
		Header: map[string][]string{},
	}
	return triggerResult, nil
}

func (p *Plugin) Action(l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	switch ma.Command {

	case "app-optics-trigger-alert-webhook":
		return app_optics_trigger_alert_webhook(p, l, ma)

	case "azure-monitor-trigger-alert-webhook":
		return azure_monitor_trigger_alert_webhook(p, l, ma)

	case "bitbucket-trigger-webhook-pr":
		return bitbucket_trigger_webhook_pr(p, l, ma)

	case "bitbucket-trigger-webhook-push":
		return bitbucket_trigger_webhook_push(p, l, ma)

	case "datadog-trigger-alert_webhook":
		return datadog_trigger_alert_webhook(p, l, ma)

	case "dynatrace-trigger-alert_webhook":
		return dynatrace_trigger_alert_webhook(p, l, ma)

	case "elementor-trigger-form-submit":
		return elementor_trigger_form_submit(p, l, ma)

	case "epsagon-trigger-alert_webhook":
		return epsagon_trigger_alert_webhook(p, l, ma)

	case "git-lab-trigger-mr-webhook":
		return git_lab_trigger_mr_webhook(p, l, ma)

	case "git-lab-trigger-webhook-push":
		return git_lab_trigger_webhook_push(p, l, ma)

	case "github-trigger-webhook-pr":
		return github_trigger_webhook_pr(p, l, ma)

	case "github-trigger-webhook-push":
		return github_trigger_webhook_push(p, l, ma)

	case "jira-trigger-issue-update-webhook":
		return jira_trigger_issue_update_webhook(p, l, ma)

	case "jira-trigger-new-issue-webhook":
		return jira_trigger_new_issue_webhook(p, l, ma)

	case "kaholo-trigger-lacework-alert-webhook":
		return kaholo_trigger_lacework_alert_webhook(p, l, ma)

	case "logic-monitor-trigger-alert-webhook":
		return logic_monitor_trigger_alert_webhook(p, l, ma)

	case "logzio-trigger-alert_webhook":
		return logzio_trigger_alert_webhook(p, l, ma)

	case "monday-trigger-status_update_webhook":
		return monday_trigger_status_update_webhook(p, l, ma)

	case "oci-monitoring-trigger-alert-webhook":
		return oci_monitoring_trigger_alert_webhook(p, l, ma)

	case "okta-trigger-alert":
		return okta_trigger_alert(p, l, ma)

	case "okta-trigger-verify":
		return okta_trigger_verify(p, l, ma)

	case "pager-duty-trigger-incident_webhook":
		return pager_duty_trigger_incident_webhook(p, l, ma)

	case "pingdom-trigger-webhook-alert":
		return pingdom_trigger_webhook_alert(p, l, ma)

	case "prometheus-trigger-alert-webhook":
		return prometheus_trigger_alert_webhook(p, l, ma)

	case "prtg-trigger-post-notification":
		return prtg_trigger_post_notification(p, l, ma)

	case "send-grid-trigger-event":
		return send_grid_trigger_event(p, l, ma)

	case "slack-button-trigger-webhook-button":
		return slack_button_trigger_webhook_button(p, l, ma)

	case "sumologic-trigger-alert-webhook":
		return sumologic_trigger_alert_webhook(p, l, ma)

	case "unbounce-trigger-webhook-submit":
		return unbounce_trigger_webhook_submit(p, l, ma)

	case "vsts-trigger-pr-webhook":
		return vsts_trigger_pr_webhook(p, l, ma)

	case "vsts-trigger-push-webhook":
		return vsts_trigger_push_webhook(p, l, ma)

	case "zabbix-trigger-webhook":
		return zabbix_trigger_webhook(p, l, ma)

	case "zapier-trigger-webhook-post":
		return zapier_trigger_webhook_post(p, l, ma)

	case "zendesk-trigger-webhook":
		return zendesk_trigger_webhook(p, l, ma)

	default:
		return nil, utils.MakeError(404, fmt.Sprintf("Unknown command: %s", ma.Command))
	}
}

func main() {
	plugin.InitApp("triggers", "Provides the triggers", version, &def, &Plugin{})
	err := plugin.App.Execute()
	if err != nil {
		log.Printf("Plugin execution failed: %v", err)
		os.Exit(1)
	}
}

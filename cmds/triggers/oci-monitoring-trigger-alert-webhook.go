package main

/*
  Name: oci-monitoring-trigger-alert-webhook
  Description: Alert
  Documentation: Alert
  Path: kaholo-trigger-oci-montiring/src/config.json

const minimatch = require("minimatch");
const parsers = require("./parsers");
const fetch = require("node-fetch");

async function alertWebhook(req, res, settings, triggerControllers) {
  try {
    const body = req.body;
    if (body.ConfirmationURL){
      const result = await fetch(body.ConfirmationURL);
      res.send("OK");
      return result;
    }
    const sevirity = parsers.severity(body.severity);
    const name = body.title;
    triggerControllers.forEach((trigger) => {
      let {alertNamePat, minAlertSeverity, maxAlertSeverity} = trigger.params;
      minAlertSeverity = parsers.severity(minAlertSeverity || "info");
      maxAlertSeverity = parsers.severity(maxAlertSeverity || "critical");

      if (alertNamePat && !minimatch(name, alertNamePat)) return;
      if (sevirity < minAlertSeverity || sevirity > maxAlertSeverity) return;
      trigger.execute(name, body);
    });
    res.status(200).send("OK");
  }
  catch (err){
    res.status(422).send(err.message);
  }
}

module.exports = {
  alertWebhook
};

*/

import (
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
)

func oci_monitoring_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	return notImplementedMessage(p, l, ma)
}

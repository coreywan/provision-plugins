package main

/*
  Name: bitbucket-trigger-webhook-push
  Description: Bitbucket Push
  Documentation: Bitbucket Push
  Path: kaholo-trigger-bitbucket/src/config.json

const minimatch = require("minimatch");

async function webhookPush(req, res, settings, triggerControllers) {
    const body = req.body;
    if (!body.repository) {
      return res.status(400).send(`Repository not found!`);
    }
    if (!body.push) {
      return res.status(400).send(`Not a push`);
    }
    try {
      const reqRepoName = body.repository.name; // Get repo name
      const push = body.push.changes[0];
      const name = push.new.name; // get branch/tag name
      const isTag = push.new.type == "tag"; // check if tag

      triggerControllers.forEach(trigger => {
        const {repoName} = trigger.params;
        if (repoName && reqRepoName !== repoName) return;
        const pattern = trigger.params[isTag ? "tagPat" : "branchPat"];
        if (!pattern || !minimatch(name, pattern)) return;
        const msg = `${reqRepoName} ${isTag ? "Tag" : "Branch"} Push`;
        trigger.execute(msg, body);
      });
      res.status(200).send("OK");
    }
    catch (err){
      res.status(422).send(err.message);
    }
}

module.exports = {
    webhookPush,
    webhookPR
};

*/

import (
	"encoding/json"
	"fmt"
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
	"regexp"
)

type bitbucketPush struct {
	changes []bitbucketChange `json:"changes"`
}

type bitbucketChange struct {
	new bitbucketChangeNew `json:"new"`
}

type bitbucketChangeNew struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

func bitbucket_trigger_webhook_push(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	triggerInput, err := getTriggerInput(ma.Params)
	if err != nil {
		return nil, err
	}

	triggerResult := &models.TriggerResult{
		Data:   []byte("OK"),
		Code:   200,
		Header: map[string][]string{},
	}

	body := &bitbucketPRData{}
	if jerr := json.Unmarshal(triggerInput.Data, &body); jerr != nil {
		triggerResult.Code = 422
		triggerResult.Data = []byte(fmt.Sprintf("JSON ERROR: %v", jerr))
		return triggerResult, nil
	}

	reqRepoName := body.repository.name
	push := body.push.changes[0]
	name := push.new.Name
	isTag := push.new.Type == "tag"

	for _, t := range triggerInput.Triggers {
		params, err := p.GetTriggerParams(t.Name)
		if err != nil {
			l.Errorf("Error getting %s trigger params: %v", err)
			continue
		}
		if anp, ok := params["bitbucket-trigger/webhook-push/repo-name"]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(reqRepoName) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
			}
		}
		pname := "bitbucket-trigger/webhook-push/tag-pat"
		if !isTag {
			pname = "bitbucket-trigger/webhook-push/branch-pat"
		}
		if anp, ok := params[pname]; ok {
			s := anp.(string)
			if r, rerr := regexp.Compile(s); rerr == nil {
				if !r.MatchString(name) {
					continue
				}
			} else {
				l.Errorf("Trigger %s has bad repo-name pattern: %v", rerr)
			}
		}

		plugin.Publish("trigger", "fired", t.Name, body)
	}

	return triggerResult, nil
}

package main

/*
  Name: logzio-trigger-alert_webhook
  Description: Logz.io alert webhook
  Documentation: Logz.io alert webhook
  Path: kaholo-trigger-logzio/src/config.json

function alertWebhook(req, res, settings, triggerControllers) {
    try {
        const body = req.body;
        const {alert_title: reqTitle, alert_severity: reqSeverity} = body;
        if (!reqTitle || !reqSeverity){
            return res.status(400).send("Bad Webhook Format");
        }

        triggerControllers.forEach(trigger => {
            const {ALERT_TITLE: title, SEVERITY: severity} = trigger.params;

            if (title && !minimatch(reqTitle, title)) return;
            if (severity && reqSeverity !== severity) return;

            trigger.execute(reqTitle, body);
        });
        res.status(200).send("OK");
    }
    catch (err){
      res.status(422).send(err.message);
    }
}

module.exports = {
    ALERT_WEBHOOK: alertWebhook
}


*/

import (
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/models"
)

func logzio_trigger_alert_webhook(p *Plugin, l logger.Logger, ma *models.Action) (interface{}, *models.Error) {
	return notImplementedMessage(p, l, ma)
}

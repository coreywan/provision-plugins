package main

import (
	"encoding/json"
	"github.com/hashicorp/vault/api"
	"gitlab.com/rackn/provision/v4/models"
	"net/url"
	"path"
	"sync"
	"time"
)

const (
	CleanupInterval = 24 * time.Hour
)

type Secret struct {
	Value     interface{}
	ExpiresAt int64
}

type Cache struct {
	secretLookup map[string]map[string]*Secret
	mu           *sync.Mutex

	client         *api.Client
	secretTtl      time.Duration
	vaultKvVersion string

	cleaner *time.Ticker
	done    chan bool
}

func CreateCache(vaultClient *api.Client, secretTtl time.Duration, vaultKvVersion string) *Cache {
	cache := &Cache{
		client:         vaultClient,
		secretTtl:      secretTtl,
		vaultKvVersion: vaultKvVersion,
		secretLookup:   map[string]map[string]*Secret{},
		mu:             &sync.Mutex{},
		cleaner:        time.NewTicker(CleanupInterval),
		done:           make(chan bool),
	}
	// Start cleaner
	cache.StartCleaner()

	return cache
}

func (c *Cache) WriteSecret(machineId string, lookupUri string, value interface{}, ttl time.Duration) {
	// lock mutex
	c.mu.Lock()

	// time now
	now := time.Now()

	// check to see if machine exists in the cache already
	machineMap, ok := c.secretLookup[machineId]
	if !ok {
		secret := map[string]*Secret{}
		secret[lookupUri] = &Secret{Value: value, ExpiresAt: now.Add(ttl).Unix()}
		c.secretLookup[machineId] = secret
	} else {
		machineMap[lookupUri] = &Secret{
			Value:     value,
			ExpiresAt: now.Add(ttl).Unix(),
		}
	}

	// unlock
	c.mu.Unlock()
}

func (c *Cache) Get(machineId string, lookupUri string) (interface{}, error) {
	c.mu.Lock()
	now := time.Now().Unix()
	it, ok := c.secretLookup[machineId][lookupUri]
	if ok {
		if now >= it.ExpiresAt {
			response, err := c.fetchSecretFromVault(lookupUri, machineId)
			if err != nil {
				return nil, err
			}
			it.Value = response
		}
	} else {
		response, err := c.fetchSecretFromVault(lookupUri, machineId)
		if err != nil {
			return nil, err
		}
		// unlock mutex and write secret
		c.mu.Unlock()
		c.WriteSecret(machineId, lookupUri, response, c.secretTtl)

		return response, nil
	}
	c.mu.Unlock()

	return it.Value, nil
}

func (c *Cache) StartCleaner() {
	go func() {
		for {
			select {
			case <-c.done:
				return
			case <-c.cleaner.C:
				c.clean()
			}
		}
	}()
}

// clean: cleans up the expired secrets from the map
func (c *Cache) clean() {
	// current time
	now := time.Now().Unix()

	// lock for cleanup
	c.mu.Lock()

	// iterating over the secrets
	for uuid, secretMap := range c.secretLookup {
		for uri, secret := range secretMap {
			if now >= secret.ExpiresAt {
				delete(secretMap, uri)
			}
		}
		if len(secretMap) == 0 {
			delete(c.secretLookup, uuid)
		}
	}

	// releasing the lock
	c.mu.Unlock()

}

// empty: empties the contents of the cache
func (c *Cache) empty() {
	// lock for cleanup
	c.mu.Lock()

	// iterating over the secrets
	for key, _ := range c.secretLookup {
		delete(c.secretLookup, key)
	}

	// releasing the lock
	c.mu.Unlock()
}

func (c *Cache) clearMachineCache(machineId string) {
	// lock for cleanup
	c.mu.Lock()

	// delete all secrets for th
	for uuid, _ := range c.secretLookup {
		if uuid == machineId {
			delete(c.secretLookup, uuid)
		}
	}

	// releasing the lock
	c.mu.Unlock()
}

func (c *Cache) getVaultPrefix() string {
	if c.vaultKvVersion == VaultKvVersion1 {
		return VaultV1PathPrefix
	} else {
		return VaultV2PathPrefix
	}
}

func (c *Cache) isKvVersion1() bool {
	return c.vaultKvVersion == VaultKvVersion1
}

func (c *Cache) fetchSecretFromVault(lookupUri string, machineId string) (interface{}, *models.Error) {
	err := &models.Error{Type: "plugin", Model: "", Key: "vault"}

	// Parse the lookup URI and get the scheme and host
	// example: "plugin-name://secret-key?path=path-to-secret"
	uri, verr := url.ParseRequestURI(lookupUri)
	if verr != nil {
		err.Code = 400
		err.Errorf("fetch secret: invalid lookupUri provided")
		return "", err
	}

	var vault VaultOptions
	vault.key = uri.Host + uri.Path
	vault.path = path.Join(c.getVaultPrefix(), uri.Query()["path"][0])

	if uri.Query()["format"] != nil {
		vault.format = uri.Query()["format"][0]
	}

	secret, verr := c.client.Logical().Read(vault.path)
	if verr != nil {
		err.Code = 500
		err.Errorf(err.Error())
		return "", err
	}

	if secret == nil {
		err.Code = 404
		err.Errorf("Secret %s not found in vault. Check the value of vault/kv-version.", uri.Query()["path"][0])
		return "", err
	}

	var vaultResult string
	var ok bool
	if c.isKvVersion1() {
		vaultResult, ok = secret.Data[vault.key].(string)
		if !ok {
			err.Code = 400
			err.Errorf("fetch secret: key %s does not exist in v1 kv engine", vault.key)
			return "", err
		}
	} else {
		m, v2ok := secret.Data["data"].(map[string]interface{})
		if !v2ok {
			err.Code = 400
			err.Errorf("fetch secret: unable to fetch secret data")
			return "", err
		}

		vaultResult, ok = m[vault.key].(string)
		if !ok {
			err.Code = 400
			err.Errorf("fetch secret: key %s does not exist in v2 kv engine", vault.key)
			return "", err
		}
	}

	var finalResult interface{}
	// If a format is specified, then we will also need convert data
	switch vault.format {
	case "json":
		verr = json.Unmarshal([]byte(vaultResult), &finalResult)
		if verr != nil {
			// There was an error converting the value to json, most like it is not valid json so just return
			return vaultResult, nil
		}
	default:
		finalResult = vaultResult
	}

	return finalResult, nil
}

package main

//go:generate sh -c "cd content ; drpcli contents bundle ../content.go"
//go:generate sh -c "cd content ; drpcli contents bundle ../content.yaml"
//go:generate sh -c "drpcli contents document content.yaml > simple-secrets.rst"
//go:generate rm content.yaml

import (
	"encoding/json"
	"fmt"
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision-plugins/v4"
	"gitlab.com/rackn/provision-plugins/v4/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin"
	"net/url"
	"os"
)

type SimpleSecret struct {
	key    string
	config map[string]string
}

var defaultConfig = map[string]string{
	"foo":   "bar",
	"apple": "pie",
}

var (
	version = v4.RSVersion
	def     = models.PluginProvider{
		Name:          "simple-secrets",
		Version:       version,
		AutoStart:     true,
		PluginVersion: 4,
		AvailableActions: []models.AvailableAction{
			{Command: "decrypt",
				Model: "machines",
				RequiredParams: []string{
					"decrypt/lookup-uri",
				},
				OptionalParams: []string{
					"simple-secrets/config",
				},
			},
		},
		Content: contentYamlString,
	}
)

func prettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}

func (p *SimpleSecret) decryptAction(l logger.Logger, model interface{}, params map[string]interface{}) (string, *models.Error) {
	lookupUri := utils.GetParamOrString(params, "decrypt/lookup-uri", "foo")
	// Parse the lookup URI and get the scheme and host
	uri, err := url.ParseRequestURI(lookupUri)
	if err != nil {
		l.Errorf(err.Error())
		return "", nil
	}
	return p.config[uri.Host+uri.Path+uri.RawQuery], nil
}

func (p *SimpleSecret) Config(l logger.Logger, session *api.Client, config map[string]interface{}) *models.Error {
	l.Infof(prettyPrint(config))

	configuration := map[string]string{}
	err := &models.Error{Type: "plugin", Model: "", Key: "simple-secrets"}
	if rerr := models.Remarshal(config["simple-secrets/config"], &configuration); rerr != nil {
		err.Code = 400
		err.Errorf("Plugin %s is missing simple-secrets/config", "simple-secrets")
	}

	if configuration == nil {
		configuration = defaultConfig
	}
	p.config = configuration

	return nil
}

func (p *SimpleSecret) Action(l logger.Logger, ma *models.Action) (answer interface{}, err *models.Error) {
	switch ma.Command {
	case "decrypt":
		answer, err = p.decryptAction(l, ma.Model, ma.Params)
	default:
		err = &models.Error{Code: 404,
			Model:    "Plugin",
			Key:      "simple-secrets",
			Type:     "rpc",
			Messages: []string{fmt.Sprintf("Unknown command: %s", ma.Command)}}
	}

	return
}

func main() {
	plugin.InitApp("simple-secrets", "Returns a secret", version, &def, &SimpleSecret{})
	err := plugin.App.Execute()
	if err != nil {
		os.Exit(1)
	}
}

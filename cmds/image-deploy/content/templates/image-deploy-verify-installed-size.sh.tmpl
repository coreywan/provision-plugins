#!/usr/bin/env bash
# Verify install disk is big enough for expanded Image

{{ template "setup.tmpl" . }}

###
#  If the 'image-deploy/image-installed-size' Param is set to a value,
#  try and determine if the 'image-deploy/install-disk' is big enough
#  for the expanded image.  The author of the deployed Image must provide
#  the value of the base install disk size that is required, this is not
#  automatically derived from the image as the image may be in a compressed
#  format.
###

{{ $size := ( .Param "image-deploy/image-installed-size" ) -}}
{{ if $size -}}
echo ">>> Image installed size specified as '{{ $size }}'."
{{ else -}}
echo "No 'image-deploy/image-installed-size' value specified, skipping disk size check."
exit 0
{{ end -}}

# will this need to be adjusted to parse 'curtin/partitions' ?
DISK="{{ .ParamExpand "image-deploy/install-disk" }}"
SIZE=$(echo {{ $size }} | sed 's/^\(.*\)[MG]/\1/g')
UNIT=$(echo {{ $size }} | sed 's/.*\([MG]\)/\1/g')
# get disk size in kilobyte as base value to work with
HAVE=$(expr $(blockdev --getsize64 $DISK) / 1024)

if [[ "$HAVE" -ge 1024000 ]]
then
  HAVE_FMT="$( expr ${HAVE} / 1024 / 1024)G"
else
  HAVE_FMT="$( expr ${HAVE} / 1024)M"
fi

# convert our wanted size to kilobyte
case $UNIT in
  M) WANT=$(expr $SIZE \* 1024) ;;
  G) WANT=$(expr $SIZE \* 1024 \* 1024) ;;
  *) xiterr 1 "Bad input '{{ $size }}'; expect number followed by 'M' or 'G'."
  ;;
esac

echo ">>> Disk '$DISK' found with size '$HAVE_FMT'"

if [[ "$HAVE" -ge "$WANT" ]]
then
  echo "+++ Image with '{{ $size }}' size can be installed on '$DISK' sized '$HAVE_FMT'."
else
  xiterr 1 "Minimum size '{{ $size }}' not met by '$DISK' disk with size '$HAVE_FMT'."
fi

exit 0
